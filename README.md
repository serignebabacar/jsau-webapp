Vue.js single-page application (SPA)
====================================


Build Setup
-----------

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run build:watch:serve

# build for production with minification
npm run build

# lint all *.js and *.vue files
npm run lint

# run automated tests
npm test
```


Dépendance vue-template-compiler
--------------------------------

Il est actuellement nécessaire de spécifier la version de
`vue-template-compiler` dans le `package.json` pour garantir la stabilité de
l'application.

Cette problématique de dépendances sur Vue.js n'est pas documentée et existe
toujours actuellement dans le code de Vue, cf. https://github.com/vuejs/vue-loader/issues/560,
https://github.com/vuejs/vue-loader/issues/470 , même si ces tickets sont notés
comme *fermés*.

Normalement lorsque Node.js >= 8 est utilisé, quand le nouveau fichier
`package.lock` est commité on doit pouvoir retirer le « pinning » de
vue-template-compiler et cela devrait résoudre le problème.

Voici l'erreur si les versions de `vue` et `vue-template-compiler` ne sont pas
identiques :

```shell
$ npm run build

Vue packages version mismatch:

- vue@2.3.3
- vue-template-compiler@2.4.2

This may cause things to work incorrectly. Make sure to use the same version for both.
If you are using vue-loader@>=10.0, simply update vue-template-compiler.
If you are using vue-loader@<10.0 or vueify, re-installing vue-loader/vueify should bump vue-template-compiler to the latest.
```

Le moyen actuel de le résoudre est :

```shell
$ npm i vue@X.Y.Z -D
$ npm i vue-template-compiler@X.Y.Z -D
`vue-template-compiler
````

Il faut évaluer ce point régulièrement pour savoir si/quand il sera corrigé.
